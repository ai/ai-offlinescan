#!/usr/bin/python

from setuptools import setup, find_packages

setup(
    name='offlinescan',
    version='0.1',
    description='Detect security issues by examining backups',
    author='Autistici/Inventati',
    author_email='debian@autistici.org',
    url='http://git.autistici.org/ai/ai-offlinescan',
    install_requires=['Flask', 'Flask-Cache'],
    setup_requires=[],
    zip_safe=True,
    packages=find_packages(),
    package_data={
        'offlinescan': ['ai/templates/*.html'],
    },
    entry_points={
        'console_scripts': [
            'ofs-incremental-scan = offlinescan.incremental_scan:main',
            'ofs-clamscan = offlinescan.clamav:main',
            'ofs-probe = offlinescan.ai.probe:main',
            ],
        },
    )

