#!/usr/bin/python
#
# FastCGI receiver component of the offlinescan package.
# (Meant to be run behind authentication).
#

import ai
import json
import time
import redis
from flask import Flask, request, abort, g, render_template
from flask.ext.cache import Cache

app = Flask(__name__)
cache = Cache(config={'CACHE_TYPE': 'simple'})


@app.template_filter('formatstamp')
def formatstamp(t):
    return time.strftime('%Y/%m/%d %H:%M', time.gmtime(int(t)))


def get_redis_connection():
    return redis.Redis(
        host=app.config['REDIS_HOST'],
        password=app.config['REDIS_PASSWORD'],
        socket_timeout=10)


def _compat_json_loads(s):
    # Awful trick for backwards compatibility with very very old,
    # incorrectly-encoded entries.
    if s.startswith("{'") or s.startswith("{u'"):
        return eval(s)
    return json.loads(s)


# Retrieve all per-user data and cache it somewhere (for a short time,
# since FastCGI runners aren't long-lived anyway).
@cache.memoize(timeout=900)
def get_virus_by_user():
    virus_by_user = {}
    rdb = get_redis_connection()
    for obj in ai.LDAPObj(ai.config.base_uid).search('(&(objectClass=ftpAccount)(status=active))'):
        entries = rdb.lrange('virus:%s' % obj['ftpname'], 0, -1)
        if entries:
            virus_by_user[obj['ftpname']] = [_compat_json_loads(x) for x in entries]
    return virus_by_user


@app.before_request
def before_request():
    g.redis = get_redis_connection()


@app.route('/submit', methods=('GET', 'POST'))
def receive():
    if request.method == 'POST':
        p = g.redis.pipeline(transaction=False)
        #per_user = {}
        for user, results in request.json.iteritems():
            key = 'virus:%s' % user
            #p.ltrim(key, 0, 0)
            for r in results:
                p.lpush(key, r)
            #per_user[user] = len(results)

        # Save some incremental stats with per-user infection counts.
        #new_ptr = 'virus_stats:%f' % time.time()
        #for user, count in per_user.iteritems():
        #    p.hset(new_ptr, user, count)
        #p.set('virus_stats:head', new_ptr)

        # Store the time of last update, to monitor freshness.
        p.set('virus_stats:stamp', int(time.time()))

        p.execute()

    return 'ok'


@app.route('/')
def index():
    return render_template('index.html',
                           virus_by_user=get_virus_by_user())


def create_app():
    # The env variable name is compatible with wsgi-app-installer.
    app.config.from_envvar('APP_CONFIG', silent=True)
    cache.init_app(app)
    return app


def main():
    from flup.server.fcgi import WSGIServer
    WSGIServer(create_app()).run()


if __name__ == '__main__':
    main()
