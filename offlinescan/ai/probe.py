"""Run scan, process the results and upload them."""

import cookielib
import json
import optparse
import os
import re
import sys
import time
import urllib2
import logging

from sso.urllib2_handler import SSOProcessor
from offlinescan import clamav
from offlinescan import incremental_scan
from offlinescan import full_scan


user_info_pattern = re.compile(
    r'(?P<host>[^/]+)/home/users/investici\.org/(?P<user>[^/]+)/html-(?P<site>[^/]+)/(?P<path>.*)$')


log = logging.getLogger(__name__)


def get_credentials():
    user = os.getenv('SSO_USER', 'offlinescan')
    pwfile = os.path.expanduser(
        os.getenv('SSO_PWFILE', '~/.offlinescan.pw'))
    with open(pwfile, 'r') as fd:
        pw = fd.read().strip()
    return user, pw


def create_opener():
    username, password = get_credentials()
    jar = cookielib.CookieJar()
    return urllib2.build_opener(
        urllib2.HTTPCookieProcessor(jar),
        SSOProcessor(username=username, password=password))


def process_scan_results(scan_results):
    """Extract AI-specific site information from the scan results."""
    now = int(time.time())
    by_user = {}
    for entry in scan_results:
        m = user_info_pattern.search(entry['path'])
        if not m:
            continue
        entry.update(m.groupdict())
        entry['found_at'] = now
        by_user.setdefault(entry['user'], []).append(entry)
    return by_user


def submit(results, submit_url):
    # Run a 'ping' request first, just to make sure that the SSO
    # cookie is set *before* we do the POST request (SSO won't work on
    # the first connection if it's a POST).
    opener = create_opener()
    try:
        opener.open(submit_url).read()
    except Exception, e:
        log.error('submit(ping) error: %s', e)
        return
    try:
        req = urllib2.Request(
            submit_url,
            json.dumps(results),
            {'Content-Type': 'application/json'})
        resp = opener.open(req)
        if resp.code != 200:
            log.error('submit error: HTTP status %d', resp.code)
    except Exception, e:
        log.error('submit error: %s', e)


def main():
    logging.basicConfig(level=logging.INFO)
    parser = optparse.OptionParser(usage='%prog [<OPTIONS>] <PATH>...')
    parser.add_option(
        '--full', dest='full_scan', action='store_true',
        help='Run a full scan, not an incremental one')
    parser.add_option(
        '--include', dest='includes', metavar='PATTERN',
        action='append', default=[],
        help='Only include files matching a pattern (the paths will be relative '
        'to the backup root)')
    parser.add_option(
        '--exclude', dest='excludes', metavar='PATTERN',
        action='append', default=[],
        help='Exclude files matching a pattern (the paths will be relative '
        'to the backup root)')
    parser.add_option(
        '--submit-url', dest='submit_url', metavar='URL',
        default='https://offlinescan.autistici.org/submit',
        help='URL to submit results to (default: %default)')
    opts, args = parser.parse_args()
    if not args:
        parser.error('Must specify a PATH')

    scanfn = incremental_scan.scan_dirs
    if opts.full_scan:
        scanfn = full_scan.scan_dirs

    files = scanfn(args, opts.includes, opts.excludes)
    scan_results = clamav.clamscan(files)
    if scan_results:
        data = process_scan_results(scan_results)
        log.info('found new viruses:\n%s', json.dumps(data, indent=4))
        submit(data, opts.submit_url)


if __name__ == '__main__':
    main()
