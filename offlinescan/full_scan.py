import fnmatch
import os


def match_any(path, patterns):
    for p in patterns:
        if fnmatch.fnmatch(path, p):
            return True
    return False


def scan_dir(root_dir, includes, excludes, changed):
    if not root_dir.endswith('/'):
        root_dir = root_dir + '/'
    for dirpath, dirnames, filenames in os.walk(root_dir):
        relpath = dirpath[len(root_dir):]
        for filename in filenames:
            path = os.path.join(relpath, filename)
            if ((excludes and match_any(path, excludes)) or
                (includes and not match_any(path, includes))):
                continue
            changed.add(os.path.join(root_dir, path))


def scan_dirs(dirs, includes=[], excludes=[]):
    """Run a full recursive scan on 'dirs'."""
    changed = set()
    for root_dir in dirs:
        scan_dir(root_dir, includes, excludes, changed)
    return changed

