import os
import unittest
from offlinescan.incremental_scan import *


class IncrementalScanTest(unittest.TestCase):

    def setUp(self):
        self.testroot = os.path.join(os.path.dirname(__file__), 'testdata')

    def tearDown(self):
        try:
            os.remove(os.path.join(
                self.testroot, 'rdiff-backup-data/.offlinescan_stamp'))
        except:
            pass

    def _fq(self, inset):
        return set(os.path.join(self.testroot, f) for f in inset)

    def test_scan_dirs(self):
        result = scan_dirs([self.testroot], [], [])
        self.assertEquals(
            self._fq(set(['testfile', 'lot of whitespaces'])),
            result)

    def test_excludes(self):
        result = scan_dirs([self.testroot], [], ['test*'])
        self.assertEquals(
            self._fq(set(['lot of whitespaces'])),
            result)

    def test_includes(self):
        result = scan_dirs([self.testroot], ['test*'], [])
        self.assertEquals(
            self._fq(set(['testfile'])),
            result)

    def test_scan_dirs_fails_on_bad_dir(self):
        self.assertRaises(Exception,
                          scan_dirs,
                          [self.testroot + '/..'], [], [])
