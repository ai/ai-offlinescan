import fnmatch
import gzip
import optparse
import os


def read_stamp(stamp_file):
    try:
        with open(stamp_file) as fd:
            return fd.read().strip()
    except:
        return ''


def match_any(path, patterns):
    for p in patterns:
        if fnmatch.fnmatch(path, p):
            return True
    return False


def scan_mirror_metadata(root_dir, mm_path, includes, excludes, changed):
    """Extract filenames from a mirror_metadata diff file."""
    with gzip.GzipFile(mm_path, 'r') as fd:
        for line in fd:
            if line.startswith('File '):
                path = line.rstrip('\n')[5:]
                if ((excludes and match_any(path, excludes)) or
                    (includes and not match_any(path, includes))):
                    continue
                changed.add(os.path.join(root_dir, path))


def scan_dir(root_dir, includes, excludes, changed):
    """Analyze rdiff-backup metadata to look for changed files.

    The scan is incremental: the timestamp of the most recent backup
    is stored in a state file in the rdiff-backup-data directory.
    """
    data_dir = os.path.join(root_dir, 'rdiff-backup-data')
    if not os.path.isdir(data_dir):
        raise Exception('%s does not seem to contain a rdiff backup' % root_dir)

    stamp_file = os.path.join(data_dir, '.offlinescan_stamp')
    last_scanned_at = read_stamp(stamp_file)

    stamp = None
    for filename in sorted(os.listdir(data_dir)):
        if (not filename.startswith('mirror_metadata.') or
            not filename.endswith('.diff.gz')):
            continue
        stamp = filename.split('.')[1]
        if stamp < last_scanned_at:
            continue
        scan_mirror_metadata(
            root_dir, os.path.join(data_dir, filename), includes, excludes, changed)

    if stamp:
        with open(stamp_file, 'w') as fd:
            fd.write('%s\n' % stamp)


def scan_dirs(dirs, includes=[], excludes=[]):
    """Analyze multiple rdiff-backup dirs."""
    changed = set()
    for root_dir in dirs:
        scan_dir(root_dir, includes, excludes, changed)
    return changed


def main():
    parser = optparse.OptionParser(usage='%prog <PATH>...')
    parser.add_option(
        '--include', dest='includes', metavar='PATTERN',
        action='append', default=[],
        help='Only include files matching a pattern (the paths will be relative '
        'to the backup root)')
    parser.add_option(
        '--exclude', dest='excludes', metavar='PATTERN',
        action='append', default=[],
        help='Exclude files matching a pattern (the paths will be relative '
        'to the backup root)')
    opts, args = parser.parse_args()
    if not args:
        parser.error('Must specify a PATH')

    for path in scan_dirs(args, opts.includes, opts.excludes):
        print path


if __name__ == '__main__':
    main()
