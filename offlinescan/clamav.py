import os
import re
import subprocess


max_cmdlen = os.sysconf('SC_ARG_MAX') / 2
devnull = open('/dev/null', 'w')
row_pattern = re.compile(r'^(.*): (.*) FOUND$')
clamscan_cmd = ['clamscan', '--no-summary', '--stdout', '--infected']


def clamscan(files):
    """Run 'clamscan' on the given files.

    Returns a list of dictionaries with 'path' and 'virus' keys.
    """
    state = {'buf': clamscan_cmd[:], 'len': 0}
    results = []

    def _flush():
        pipe = subprocess.Popen(state['buf'],
                                stdout=subprocess.PIPE,
                                stderr=devnull)
        for line in pipe.stdout:
            m = row_pattern.match(line.rstrip('\n'))
            if m:
                results.append({'path': m.group(1), 'virus': m.group(2)})
        pipe.wait()
        state['buf'] = clamscan_cmd[:]
        state['len'] = 0

    # Build argument lists, xargs-style.
    for f in files:
        state['buf'].append(f)
        state['len'] += len(f) + 1
        if state['len'] > max_cmdlen:
            _flush()
    if state['len']:
        _flush()

    return results


def main():
    import json, sys
    files = map(lambda x: x.rstrip('\n'), sys.stdin)
    json.dump(clamscan(files), sys.stdout, indent=2)


if __name__ == '__main__':
    main()
